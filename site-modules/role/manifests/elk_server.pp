class role::elk_server {
    include ::profile::base_linux
    include ::profile::dns::client
    include ::profile::consul::server
    include ::profile::elk::elasticsearch
    include ::profile::elk::logstash
    include ::profile::elk::kibana 
}
# If the elk components had their own dedicated server
# they need their own role:
#
# manifest/elasticsearch.pp have to be create with following content:
# class role::elasticsearch_server {
#   include ::profile::base_linux
#   include ::profile::dns::client
#   include ::profile::consul::server
#   include ::profile::elk::elasticsearch
#}
#
# manifest/logstash.pp have to be create with following content:
# class role::logstash_server {
#   include ::profile::base_linux
#   include ::profile::dns::client
#   include ::profile::consul::server
#   include ::profile::elk::logstash
#}
# 
# manifest/kibana.pp have to be create with following content:
# class role::kibana_server {
#   include ::profile::base_linux
#   include ::profile::dns::client
#   include ::profile::consul::server
#   include ::profile::elk::kibana
#}
