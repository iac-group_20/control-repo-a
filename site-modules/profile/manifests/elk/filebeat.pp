class profile::elk::filebeat {
  
  $elk_server = lookup('profile::elk::elk_server')

  # If logstash was running on its own virtual machine.
  # $logstash_server = lookup('profile::elk::logstash')

 class { 'filebeat':
    outputs => {
      'logstash' => {
      # 'hosts'                       => [ "${logstash_server}:5044" ],  
        'hosts'                       => [ "${elk_server}:5044" ],
        'ssl'                         => { 
        'certificate_authorities'     => '/etc/ssl/certs/logstashCert.crt',
        },
      },
    },
    package_ensure => 'present',
    service_ensure => 'running',
    service_enable => true,
  }

  filebeat::input { 'syslogs':
  paths    => [
    '/var/log/auth.log',
    '/var/log/syslog',
  ],
  doc_type     => 'syslog-beat',
  ignore_older => '72h',
}
}
