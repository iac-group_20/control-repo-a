class profile::elk::winlogbeat {

$elk_server = lookup('profile::elk::elk_server')

# If logstash was running on its own virtual machine.
# $logstash_server = lookup('profile::elk::logstash')


class { 'winlogbeat':
 package_ensure => 'present',
 service_ensure => 'running',
 service_enable => true,
  outputs => {
    'logstash'     => {
     'hosts' => [
     # "${logstash_server}:5044"
       "${elk_server}:5044"
     ],
     'index'                        => 'winlogbeat',
     'ssl.certificate.authorities'  => [ "C:/Programfiles/winlogbeat/ssl/logstashCert.crt" ]
    },
  },
  event_logs => {
  'Application' => { 'ignore_older' => '72h' },
  'System'      => { 'ignore_older' => '24h' },
  'Security'    => { 'ignore_older' => '24h' },
  }
 }
}
