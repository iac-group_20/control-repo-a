#!/bin/bash

# Ensure everything is installed before running the script
# The script is ran as the last task in the lib/manager_boot.bash file
# Line 79 initializes the script for lib/manager_boot.bash
sleep 5m

# Make sure only root have access
chmod 700 /etc/ssl
cd /etc/ssl
mkdir requests
# etc/ssl/{private,certs} should already exist

# Generate private key for CA
openssl genrsa -aes256 -out private/CA.key 2048

# Generate root certificate using the private key
openssl req -x509 -new -nodes -subj "/C=NO/ST=Innlandet/L=Gjovik/O=Group20/CN=elk.node.consul" -key private/CA.key -days 1826 -out certs/CA.crt

# Generate private key
openssl genrsa -aes256 -out private/logstash_priv.key 2048

# Generate a certificate signing request (CSR) with private key
openssl req -new -nodes -subj "/C=NO/ST=Innlandet/L=Gjovik/O=Group20/CN=elk.node.consul" -key private/logstash_priv.key -out requests/logstash_request.csr

# Generating certificates using the CSR
openssl x509 -req -in requests/logstash_requst.csr -CA private/CA.key -CAkey certs/CA.crt -CAcreateserial -out certs/logstashCert.crt -days 1826

# Using scp to copy and move the certificate to the host nodes that are communicating with logstash
scp certs/logstashCert.crt root@lin1.node.consul:/root/ssh/certs
scp certs/logstashCert.crt root@lin2.node.consul:/root/ssh/certs

scp certs/logstashCert.crt Administrator@win1.node.consul:/C:/Programfiles/winlogbeat/ssl/
scp certs/logstashCert.crt Administrator@win2.node.consul:/C:/Programfiles/winlogbeat/ssl/
