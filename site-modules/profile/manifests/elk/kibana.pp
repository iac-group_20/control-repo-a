class profile::elk::kibana {

  $elk_server = lookup('profile::elk::elk_server')

  # If kibana was running on its own virtual machine.
  # $kibana_server = lookup('profile::elk::kibana')
  # $elasticsearch_server = lookup('profile::elk::elasticsearch')

 include ::java
 include elastic_stack::repo

 class { 'kibana':
  ensure => 'present',
    config => {
      'server.port' => '5601',
    # 'server.name' => [ "${kibana_server}" ],
      'server.name' => 'localhost',
    # 'elasticsearch.hosts' => [ "${elasticsearch_server}" ],
      'elasticsearch.hosts' => [ "http://localhost:9200" ],
      'elasticsearch.username' => "kibanadmin",
      # Probably not the best practice for password handling, using hiera e-yaml could be a better option.
      'elasticsearch.password' => lookup('profile::elk::elk::password')
  }
 }
 
   class {'nginx': }
# nginx::resource::server{ "${kibana_server}":
  nginx::resource::server{ "${elk_server}":
    listen_port => 80,
    proxy       => 'http://localhost:5601',
  }
 }

