node default {
  notify { "Oops Default! I'm ${facts['hostname']}": }
}

node 'manager.node.consul' {
  include ::role::manager_server
}

node /win1/ {
  include ::role::domain_joined_server
}

node /win2/ {
  include ::role::domain_joined_server
}

node 'lin1.node.consul' {
  include ::role::linux_server
}

node 'lin2.node.consul' {
  include ::role::linux_server
}

node 'dir.node.consul' {
  include ::role::directory_server
}

node 'mon.node.consul' {
  include ::role::monitoring_server
}

node 'elk.node.consul' {
  include ::role::elk_server
}
#
# If each elk component was running on its dedicated server:
#
# node 'elasticsearch.node.consul' {
# include ::role::elasticsearch_server
# }
# node 'logstash.node.consul' {
# include ::role::logstash_server
# }
# node 'kibana.node.consul' {
# include ::role::kibana_server
# }
