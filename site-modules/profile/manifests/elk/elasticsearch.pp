class profile::elk::elasticsearch {

# If elasticsearch was running on its own virtual machine.
# $elasticsearch_server = lookup('profile::elk::elasticsearch')

 include ::java
 include elastic_stack::repo

 class { 'elasticsearch':
  ensure => 'present',
  restart_on_change => true,
   instances => {
     'es-01' => {
       'config' => {
       # 'network.host'   => [ "${elasticsearch_server}" ],
         'network.host'   => 'localhost',
         'http.port'      => '9200',
         'discovery.type' => 'single-node',
    }
   }
  }
 }
}
