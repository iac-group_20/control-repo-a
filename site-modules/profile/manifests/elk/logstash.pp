class profile::elk::logstash {

 include ::java
 include elastic_stack::repo

 class { 'logstash': 
  ensure => 'present',
}
  # Point to logstash.conf file in site-modules/profile/files/
  # The file contains input, filter and output for logstash
  logstash::configfile { 'logstash.conf':
   source => 'puppet:///modules/profile/logstash.conf',
 }
}
